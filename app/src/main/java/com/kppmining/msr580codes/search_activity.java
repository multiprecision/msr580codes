package com.kppmining.msr580codes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class search_activity extends Activity
{
    private Spinner spinner_system;
    private Button button_search;
    private EditText edit_code;
    private TextView text_not_found;

    private static final String[] systems = {"COO", "EMS", "GMS", "RET", "CUV", "ICL", "BMS", "APS", "TCO"};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        spinner_system = (Spinner) findViewById(R.id.spinner_system);
        button_search = (Button) findViewById(R.id.button_search);
        edit_code = (EditText) findViewById(R.id.edit_code);
        text_not_found = (TextView) findViewById(R.id.text_not_found);

        // Set spinner values.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(search_activity.this, android.R.layout.simple_spinner_item, systems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_system.setAdapter(adapter);

        if (shared_data.get_instance().data_loaded)
        {
            text_not_found.setText("Successfully loaded " + String.valueOf(shared_data.get_instance().data.size()) + " items.");
        }

        // Set button click listener.
        button_search.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Search through the data
                int index = -1;
                for (int i = 0; i < shared_data.get_instance().data.size(); i++)
                {
                    data_column row= shared_data.get_instance().data.get(i);
                    if (row.system.equals(spinner_system.getSelectedItem().toString()) &&
                            row.code.equals(edit_code.getText().toString()))
                    {
                        index = i;
                        break;
                    }
                }
                if (index >= 0)
                {
                    text_not_found.setText("");
                    shared_data.get_instance().result = shared_data.get_instance().data.get(index);
                    startActivity(new Intent(search_activity.this, search_result_activity.class));
                }
                else
                {
                    text_not_found.setText("Not found!");
                }
            }
        });

    }
}

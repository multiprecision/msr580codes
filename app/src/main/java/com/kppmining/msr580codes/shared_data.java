package com.kppmining.msr580codes;

import java.util.ArrayList;

public class shared_data
{
    private static shared_data instance = null;

    public data_column result;

    public ArrayList<data_column> data;

    public boolean data_loaded;

    protected shared_data()
    {
        data = new ArrayList<>();
        data_loaded = false;
    }

    public static synchronized shared_data get_instance()
    {
        if(instance == null)
        {
            instance = new shared_data();
        }
        return instance;
    }
}
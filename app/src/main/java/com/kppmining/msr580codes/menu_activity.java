package com.kppmining.msr580codes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class menu_activity extends Activity
{

    Button goto_search_button;
    Button goto_profile_button;
    Button goto_about_button;
    Button goto_contact_button;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        goto_search_button = (Button) findViewById(R.id.button_goto_search);
        goto_profile_button = (Button) findViewById(R.id.button_goto_profile);
        goto_about_button = (Button) findViewById(R.id.button_goto_about);
        goto_contact_button = (Button) findViewById(R.id.button_goto_contact);

        goto_search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(menu_activity.this, search_activity.class));
            }
        });

        goto_profile_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(menu_activity.this, profile_activity.class));
            }
        });

        goto_about_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(menu_activity.this, about_activity.class));
            }
        });

        goto_contact_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(menu_activity.this, contact_activity.class));
            }
        });

        // Load data from JSON.
        if (!shared_data.get_instance().data_loaded)
        {
            // Read data.json from raw resources.
            InputStream istream = getResources().openRawResource(R.raw.data);
            ByteArrayOutputStream ostream = new ByteArrayOutputStream();

            int next;
            try
            {
                next = istream.read();
                while (next != -1)
                {
                    ostream.write(next);
                    next = istream.read();
                }
                istream.close();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                shared_data.get_instance().data_loaded = false;
            }

            // Parse JSON.
            try
            {
                JSONObject json_obj = new JSONObject(ostream.toString());
                JSONArray json_arr = json_obj.getJSONArray("data");
                for (int i = 0; i < json_arr.length(); i++)
                {
                    data_column col = new data_column();
                    col.system = json_arr.getJSONObject(i).getString("system");
                    col.code = json_arr.getJSONObject(i).getString("code");
                    col.heading = json_arr.getJSONObject(i).optString("heading");
                    col.description = json_arr.getJSONObject(i).optString("description");
                    col.cause = json_arr.getJSONObject(i).optString("cause");
                    col.notes = json_arr.getJSONObject(i).optString("notes");
                    col.action = json_arr.getJSONObject(i).optString("action");
                    shared_data.get_instance().data.add(col);
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                shared_data.get_instance().data_loaded = false;
            }

            shared_data.get_instance().data_loaded = true;
        }
    }
}

package com.kppmining.msr580codes;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class search_result_activity extends Activity
{
    private Button button_back;
    private TextView text_heading;
    private TextView text_description;
    private TextView text_cause;
    private TextView text_notes;
    private TextView text_action;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        button_back = (Button) findViewById(R.id.button_back);
        text_heading = (TextView) findViewById(R.id.text_heading);
        text_description = (TextView) findViewById(R.id.text_description);
        text_cause = (TextView) findViewById(R.id.text_cause);
        text_notes = (TextView) findViewById(R.id.text_notes);
        text_action = (TextView) findViewById(R.id.text_action);

        button_back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        data_column col = shared_data.get_instance().result;
        text_heading.setText(col.system + col.code + " " + col.heading);
        text_description.setText(col.description);
        text_cause.setText(col.cause);
        text_notes.setText(col.notes);
        text_action.setText(col.action);
    }
}
